﻿using UnityEngine;
using System.Collections;

public abstract class BasicMazeGenerator
{
	public int RowCount{ get{ return numMazeRows; } }
	public int ColumnCount { get { return numMazeColumns; } }

	private int numMazeRows;
	private int numMazeColumns;
	private MazeCell[,] myMaze;

	public BasicMazeGenerator(int rows, int columns)
    {
		numMazeRows = Mathf.Abs(rows);
		numMazeColumns = Mathf.Abs(columns);
		if (numMazeRows == 0)
        {
			numMazeRows = 1;
		}
		if (numMazeColumns == 0)
        {
			numMazeColumns = 1;
		}
		myMaze = new MazeCell[rows,columns];
		for (int row = 0; row < rows; row++)
        {
			for(int column = 0; column < columns; column++)
            {
				myMaze[row,column] = new MazeCell();
			}
		}
	}

	public abstract void GenerateMaze();

	public MazeCell GetMazeCell(int row, int column)
    {
        if (row >= 0 && column >= 0 && row < numMazeRows && column < numMazeColumns)
        {
            return myMaze[row, column];
        }
        else
        {
            Debug.Log(row + " " + column);
            throw new System.ArgumentOutOfRangeException();
        }
    }

	protected void SetMazeCell(int row, int column, MazeCell cell)
    {
        if (row >= 0 && column >= 0 && row < numMazeRows && column < numMazeColumns)
        {
            myMaze[row, column] = cell;
        }
        else
        {
            throw new System.ArgumentOutOfRangeException();
        }
    }
}
