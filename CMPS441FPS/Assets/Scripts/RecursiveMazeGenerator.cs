﻿using UnityEngine;
using System.Collections;

public class RecursiveMazeGenerator : BasicMazeGenerator
{

	public RecursiveMazeGenerator(int rows, int columns):base(rows,columns){}

	public override void GenerateMaze ()
	{
		checkCells (0, 0, Direction.Start);
	}

	private void checkCells(int row, int column, Direction moveMade)
    {
		Direction[] validMoves = new Direction[4];
        int numValidMoves = 1;
        //want to run this loop at least once, that's why it's starting at 1 and then going to 0
        while (numValidMoves > 0)
        {
            numValidMoves = 0;
            //see if each cell around you has been visited yet and increment valid moves and add it to array
            //of valid moves. will not revisit old cells (they get their IsVisited property set to true)
            if (column + 1 < ColumnCount && !GetMazeCell(row, column + 1).isVisited)
            {
                validMoves[numValidMoves] = Direction.Right;
                numValidMoves++;
            }
            else if (!GetMazeCell(row, column).isVisited && moveMade != Direction.Left)
            {
                GetMazeCell(row, column).rightWall = true;
            }
            if (row + 1 < RowCount && !GetMazeCell(row + 1, column).isVisited)
            {
                validMoves[numValidMoves] = Direction.Front;
                numValidMoves++;
            }
            else if (!GetMazeCell(row, column).isVisited && moveMade != Direction.Back)
            {
                GetMazeCell(row, column).frontWall = true;
            }
            if (column > 0 && column - 1 >= 0 && !GetMazeCell(row, column - 1).isVisited)
            {
                validMoves[numValidMoves] = Direction.Left;
                numValidMoves++;
            }
            else if (!GetMazeCell(row, column).isVisited && moveMade != Direction.Right)
            {
                GetMazeCell(row, column).leftWall = true;
            }
            if (row > 0 && row - 1 >= 0 && !GetMazeCell(row - 1, column).isVisited)
            {
                validMoves[numValidMoves] = Direction.Back;
                numValidMoves++;
            }
            else if (!GetMazeCell(row, column).isVisited && moveMade != Direction.Front)
            {
                GetMazeCell(row, column).backWall = true;
            }
            //marking the dead ends for enemy spawning
            if (numValidMoves == 0 && !GetMazeCell(row, column).isVisited)
            {
                GetMazeCell(row, column).deadEnd = true;
            }

            GetMazeCell(row, column).isVisited = true;

            //the recursive part of the algorithm, based on the moves you just added to the array
            //you go back and take a random move and then repeat the above from the new cell
            if (numValidMoves > 0)
            {
                switch (validMoves[Random.Range(0, numValidMoves)])
                {
                    case Direction.Start:
                        break;
                    case Direction.Right:
                        checkCells(row, column + 1, Direction.Right);
                        break;
                    case Direction.Front:
                        checkCells(row + 1, column, Direction.Front);
                        break;
                    case Direction.Left:
                        checkCells(row, column - 1, Direction.Left);
                        break;
                    case Direction.Back:
                        checkCells(row - 1, column, Direction.Back);
                        break;
                }
            }
        }
	}
}
