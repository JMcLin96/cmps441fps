﻿using UnityEngine;
using System.Collections;

public enum Direction
{
	Start,
	Right,
	Front,
	Left,
	Back,
};
public class MazeCell
{
	public bool isVisited = false;
	public bool rightWall = false;
	public bool frontWall = false;
	public bool leftWall = false;
	public bool backWall = false;
    public bool deadEnd = false;
}
