﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy2Movement : MonoBehaviour
{
    NavMeshAgent nav;
    SphereCollider col;
    private Transform target = null;
    Transform goal;
    private Vector3 initialPosition;
    private int x = 0;

    void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
        initialPosition = transform.position;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") target = other.transform;
    }
    
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") target = null;
    }


    void Update()
    {
        if (goal == null)
        {
            try { goal = GameObject.FindGameObjectWithTag("Finish").transform; }
            catch (Exception e) { }
        }

        if (target == null)
        {
            if(x > 0)
            {
                x--;
            }
            else
            {
                nav.SetDestination(goal.position);
            }
            /*alreadyMoving = false;
            transform.LookAt(target);
            float distance = Vector3.Distance(transform.position, target.position);
            bool tooClose = distance < 5;
            Vector3 direction = tooClose ? Vector3.back : Vector3.forward;
            transform.Translate(direction * Time.deltaTime);*/
        }
        else
        {
            nav.SetDestination(initialPosition);
            x = x + 5;
        }
    }
}