﻿using UnityEngine;
using System.Collections;

public class RangedEnemy : MonoBehaviour {

    public float timeBetweenAttacks = 0.5f; 
    public int attackDamage = 10;
    GameObject player;
    PlayerHP playerHealth;
    EnemyHP enemyHealth;
    bool playerInRange;
    float timer;
    Animator anim;
    RaycastHit hit;
    Vector3 rayDirection;

    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHP>();
        enemyHealth = GetComponent<EnemyHP>();
        anim = GetComponent<Animator>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = true;
        }
    }


    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = false;
        }
    }
    void Update ()
    {
        timer += Time.deltaTime;
        rayDirection = player.transform.position - transform.position;
        if(Physics.Raycast (transform.position, rayDirection, out hit))
        {
            if(hit.transform == player.transform)
            {
                if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
                {
                    Attack();
                }
            }
        }

        
    }

    void Attack ()
    {
        timer = 0f;
        if(playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
            anim.SetTrigger("Attacking");
        }
    }
}
