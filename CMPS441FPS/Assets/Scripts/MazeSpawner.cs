﻿using UnityEngine;
using System.Collections;
using System;

public class MazeSpawner : MonoBehaviour
{
	public GameObject Floor = null;
	public GameObject Wall = null;
    public GameObject Goal = null;
    public GameObject Seeker = null;
    public GameObject Enemy2 = null;
    public GameObject Automaton = null;
    public int numRows = 10;
	public int numColumns = 10;
    public int numSeekers = 3;
    public int numEnemy2 = 3;
    public int numAutomatons = 3;


	private float cellWidth = 4;
	private float cellHeight = 4;
    private bool goalInit = false;

    private BasicMazeGenerator newMaze = null;
    

    void Start ()
    {
        newMaze = new RecursiveMazeGenerator(numRows, numColumns);
        newMaze.GenerateMaze();
        //iterate through and set up the way each wall faces 
		for (int row = 0; row < numRows; row++)
        {
			for(int column = 0; column < numColumns; column++)
            {
				float x = column*(cellWidth);
				float z = row*(cellHeight);
				MazeCell cell = newMaze.GetMazeCell(row,column);
				GameObject tmp;
				tmp = Instantiate(Floor,new Vector3(x,0,z), Quaternion.Euler(0,0,0)) as GameObject;
				tmp.transform.parent = transform;
				if(cell.rightWall)
                {
					tmp = Instantiate(Wall,new Vector3(x+cellWidth/2,0,z)+Wall.transform.position,Quaternion.Euler(0,90,0)) as GameObject;
					tmp.transform.parent = transform;
				}
				if(cell.frontWall)
                {
					tmp = Instantiate(Wall,new Vector3(x,0,z+cellHeight/2)+Wall.transform.position,Quaternion.Euler(0,0,0)) as GameObject;
					tmp.transform.parent = transform;
				}
				if(cell.leftWall)
                {
					tmp = Instantiate(Wall,new Vector3(x-cellWidth/2,0,z)+Wall.transform.position,Quaternion.Euler(0,270,0)) as GameObject;
					tmp.transform.parent = transform;
				}
				if(cell.backWall)
                {
					tmp = Instantiate(Wall,new Vector3(x,0,z-cellHeight/2)+Wall.transform.position,Quaternion.Euler(0,180,0)) as GameObject;
					tmp.transform.parent = transform;
				}
                if (cell.deadEnd && ((numSeekers > 0) || (numEnemy2 > 0) || (numAutomatons > 0)))
                {
                    if((row == numRows - 1) && (column == numColumns - 1))
                    {
                        tmp = Instantiate(Goal, new Vector3(x, 0, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                        tmp.transform.parent = transform;
                        goalInit = true;
                    }
                    else
                    {
                        double RNG = UnityEngine.Random.value;
                        if ((RNG <= .3) && (numSeekers > 0) && (Seeker != null))
                        {
                            tmp = Instantiate(Seeker, new Vector3(x, 3, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                            tmp.transform.parent = transform;
                            numSeekers--;
                        }
                        else if ((RNG >= .33 && RNG <= .66) && (numEnemy2 > 0) && (Enemy2 != null))
                        {
                            tmp = Instantiate(Enemy2, new Vector3(x, 1, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                            tmp.transform.parent = transform;
                            numEnemy2--;
                        }
                        else if(Automaton != null && numAutomatons > 0)
                        {
                            tmp = Instantiate(Automaton, new Vector3(x, 1, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                            tmp.transform.parent = transform;
                            numAutomatons--;
                        }
                        else
                        {
                            if(numEnemy2 > 0 && Enemy2 != null)
                            {
                                tmp = Instantiate(Enemy2, new Vector3(x, 3, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                                tmp.transform.parent = transform;
                                numEnemy2--;
                            }
                            else if(numSeekers > 0 && Seeker != null)
                            {
                                tmp = Instantiate(Seeker, new Vector3(x, 3, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                                tmp.transform.parent = transform;
                                numSeekers--;
                            }
                        }
                    }
                }

                if((row == numRows - 1) && (column == numColumns - 1) && (goalInit == false))
                {
                    tmp = Instantiate(Goal, new Vector3(x, 1, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                    tmp.transform.parent = transform;
                }
            }
		}
	}
}
