﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHP : MonoBehaviour {

    public int startingHealth = 100;           
    public int currentHealth;
    public Image damaged;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    public Text totalHP;
    public bool isDamaged;

    void Awake()
    {
        currentHealth = startingHealth;
        totalHP.text = "Health: 100";
    }
    void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isDamaged)
        {
            damaged.color = flashColour;
        }
        else
        {
            damaged.color = Color.Lerp(damaged.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        isDamaged = false;
    }

    public void TakeDamage(int amount)
    {
        isDamaged = true;
        currentHealth -= amount;
        totalHP.text = "Health: " + currentHealth.ToString();
        
        // If the player has lost all it's health and the death flag hasn't been set yet...
        if (currentHealth <= 0)
        {
            Restart();
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
