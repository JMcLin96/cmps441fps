﻿using UnityEngine;
using System.Collections;

public class EnemyHP : MonoBehaviour {

    public int startingHealth = 10;
    public int currentHealth;


    void Awake()
    {

        currentHealth = startingHealth;
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        currentHealth -= amount;

        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }
    }
}
