﻿using UnityEngine;
using System.Collections;

public class Enemy3Movement : MonoBehaviour
{
    Transform player;               // Reference to the player's position.
    NavMeshAgent nav;               // Reference to the nav mesh agent.
    SphereCollider col;
    private Transform target = null;
    private Vector3 initialPosition;

    void Awake()
    {
        // Set up the references.
        initialPosition = transform.position;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        nav = GetComponent<NavMeshAgent>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") target = other.transform;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player") target = null;
    }


    void Update()
    {
        if(target != null)
        {
            nav.SetDestination(player.position);
        }
        else
        {
            nav.SetDestination(initialPosition);
        }
    }
}